# About
This project use [this](https://github.com/rrmhearts/linux-driver-examples) code for startup.

# Issues
If you have error of can't find the LD like this:

```
make -C /lib/modules/5.15.5-1-ARCH/build M=/tmp/rawmem modules
make[1]: Entering directory '/usr/lib/modules/5.15.5-1-ARCH/build'
  CC [M]  /tmp/rawmem/main.o
  LD [M]  /tmp/rawmem/rawmem.o
/bin/sh: line 1: aarch64-unknown-linux-gnu-ld: command not found
make[2]: *** [scripts/Makefile.build:474: /tmp/rawmem/rawmem.o] Error 127
make[1]: *** [Makefile:1868: /tmp/rawmem] Error 2
make[1]: Leaving directory '/usr/lib/modules/5.15.5-1-ARCH/build'
make: *** [Makefile:16: all] Error 2
```

Just make link manually via command: `sudo ln -s (which ld) /usr/bin/aarch64-unknown-linux-gnu-ld`

